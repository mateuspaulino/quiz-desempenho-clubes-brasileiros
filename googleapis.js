exports.get = function(opts, cb) {
	
	var google = require('googleapis');
	var request = require('request');
	var qs = require('querystring');

	var scopes = [
		'https://www.googleapis.com/auth/drive.readonly',
		'https://www.googleapis.com/auth/drive.file',
		'https://www.googleapis.com/auth/drive'
		];

	var caminhoChave = "conteudoarte-googleapis.pem";
	// console.log(process.env.GOOGLEAPIS_KEY);
		
	var jwtClient = new google.auth.JWT(
		'789212483511-ejptloidfgok4r0a7ce4ab8ct14ls5rr@developer.gserviceaccount.com',
		caminhoChave,
		null,
		scopes
		 );


	var drive = google.drive('v2');


	google.options({ auth: jwtClient });

	jwtClient.authorize(function(err, tokens){

		
		if(err) {
			console.log(err);
			return;
		}

		jwtClient.credentials = tokens;
		var file = drive.files.get({ fileId: opts.fileId}, function(err, resp){
			var url = resp.exportLinks[opts.type] + '&' +qs.stringify(tokens);
			request.get(url+'?id=' + new Date().getTime(), function(a, b, c){

				return cb(c);
						
			})

		});

	});

}
