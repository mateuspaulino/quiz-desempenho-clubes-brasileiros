var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
var del = require('del');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var fs = require('fs');
var imagemin = require('gulp-imagemin');



var pkg = JSON.parse(fs.readFileSync('package.json', 'utf8'));
var dados = JSON.parse(fs.readFileSync('dados.json', 'utf8'));

var urls3 = 'http://infograficos-estaticos.s3.amazonaws.com/';

gulp.task('default', ['jade:dev', 'sass', 'watch', 'connect']);
gulp.task('responsive', ['clean:build', 'copy', 'jade:responsive', 'sassprod','images']);
gulp.task('desk_and_mobi', ['clean:build', 'copy', 'jade:desk_and_mobi', 'sassprod']);

gulp.task('watch', function() {
  gulp.watch(['project/jade/*.jade', 'project/jade/**/*.jade', 'project/jade/**/**/*.jade'], ['jade:dev']);
  gulp.watch(['project/sass/*.scss', 'project/sass/**/*.scss'], ['sass']);
  gulp.watch(['project/static/js/*.js'], ['copyjs']);
  //gulp.watch('project/static/js/**/*.js', ['scripts']);
});

gulp.task('images', function(tmp) {
    gulp.src(['project/static/images/*.jpg', 'project/static/images/*.png', 'project/static/images/*.gif'])
        .pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
        .pipe(gulp.dest('versions/build/assets/images'));
});

gulp.task('jade:dev', function() {
  return gulp.src('project/jade/templates/development/*.jade')
    .pipe(plumber())
    .pipe(jade({
      pretty: false,
      data: {
        s3: urls3,
        amb: 'dev',
        pasta: pkg.name,
        dados: dados
      }
    }))
    .pipe(gulp.dest('versions/dev'))
    .pipe(reload({ stream:true }));
});

gulp.task('jade:responsive', function() {
  gulp.src('project/jade/templates/responsive/index.jade')
    .pipe(plumber())
    .pipe(jade({
      pretty: false,
      data: {
        s3: urls3,
        amb: 'prod',
        pasta: pkg.name,
        dados: dados
      }
    }))
    .pipe(gulp.dest('versions/build'));
});

gulp.task('jade:desk_and_mobi', function() {
  gulp.src('project/jade/templates/desk_and_mobi/index.jade')
    .pipe(plumber())
    .pipe(jade({
      pretty: false,
      data: {
        s3: urls3,
        amb: 'prod',
        pasta: pkg.name,
        dados: dados
      }
    }))
    .pipe(gulp.dest('versions/build'));
});

gulp.task('sass', function() {
  return gulp.src('project/sass/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: true
    }))
    .pipe(gulp.dest('versions/dev/assets/css'))
    .pipe(browserSync.stream({match: '**/*.css'}));
    //.pipe(connect.reload());
});

gulp.task('sassprod', function() {
  gulp.src('project/sass/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: true
    }))
    .pipe(gulp.dest('versions/build/assets/css'));
});

/*gulp.task('connect', function() {
  connect.server({
    root: 'versions/dev',
    livereload: true,
    port: 9000,
    middleware: function(connect, opt) {
      return [
        connect.static('../recursos'),
        connect.static('versions/dev'),
        connect.static('versions/dev/assets'),
        connect.static('versions/dev/assets/css'),
        connect.static('project/static')
      ]
    }
  });
});*/
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('connect', function() {
  var browserSync = require('browser-sync');

  browserSync({
    port: 9000,
    server: {
      baseDir: [
        '../recursos',
        'versions/dev', 
        'versions/dev/assets',
        'project/static'
      ]
    },
    ghostMode: false
  });
});


gulp.task('clean:build', function() {
  return del.sync(['versions/build']);
});
gulp.task('clean:dev', function() {
  return del.sync('versions/dev');
});

gulp.task('copy', function() {

  var arquivos = [
    "project/static/**/*",'!project/static/images'
  ];

  gulp.src(arquivos)
    .pipe(gulp.dest('versions/build/assets'));


});

gulp.task('copyjs', function() {

  var arquivos = [
    "project/static/js/*"
  ];

  gulp.src(arquivos)
    .pipe(gulp.dest('versions/dev/assets/js')).pipe(reload({ stream:true }));


});

gulp.task('googleapis', function(){
  var googleapis = require('./googleapis.js');
  var archieml = require('archieml');

  var opts = { 
    fileId: '15_yAKjPzWns6lIjVZPyG7Tr6uyYgKEtv0YrfGCIMzrM', 
    path:__dirname,
    type: 'text/plain'
  };


  googleapis.get(opts, function(rs){
    var json = JSON.stringify(archieml.load(rs));
    console.log(json);
    fs.writeFile(opts.path + '/dados.json', json);
  });
});