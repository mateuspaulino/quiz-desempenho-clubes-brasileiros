# Quiz - Desempenho dos clubes brasileiros

## Time - O Globo
* Mateus Paulino - Desenvolvedor Front-End
* Thaís Leão - Desenvolvedor Front-End
* Gustavo Almeida - Desenvolvedor Back-End
* Isabella Marques - Designer
* Daniel Lima - Produtor

## Demo
- Desktop [aqui](https://marvelapp.com/1j3iig0)
- Mobile [aqui](https://marvelapp.com/jcf6fg)

## Como executar o projeto
- Entre na pasta do projeto
- Baixe os arquivos, abra as pastas versions>dev e abra o arquivo index.html
- Caso seja desenvolvedor, execute os seguintes comandos via terminal (dentro da pasta)
- Obs: É preciso ter: NodeJs e Gulp instalados
```sh
$ npm install
```
```sh
$ gulp
```
- Digite localhost:9000 na barra de endereços do seu navegador para visualizar o projeto
- Caso queira gerar a versão de produção digite no seu terminal
```sh
$ gulp desk_and_mobi
```
 
## License

Copyright 2016

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
