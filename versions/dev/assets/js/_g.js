var oglobo_infograficos = function() {

  var ambiente = 'dev';

  var ambientes_permitido = ['dev', 'prod'];

  var atributos_externos = ['src', 'href'];

  var dom = {};

  var urlS3 = 'http://infograficos-estaticos.s3.amazonaws.com/';

  var urlLocal = '';

  var pasta = '';

  var url = '';

  var arquivo = {
    dev: null,
    prod: null
  };

  var arquivos = {};



  var inicia = function() {
    setAmbiente();
  }

  var getDom = function(tag) {

    // Dessa forma, se não encontrar a tag no dom, ele retorna o objeto jquery vazio (como se tivesse buscado puramente pelo jquery) e não cria a variável de cache.
    if (!dom[tag]) {
      var d = $(tag)
      if (d.length > 0) {
        dom[tag] = d;
      }
    } else {
      d = dom[tag];
    }

    return d;
  }

  var setAmbiente = function(_ambiente) {
    if (ambientes_permitido.indexOf(_ambiente) !== -1) {
      ambiente = _ambiente;
    } else {
      ambiente = 'dev';
    }
    setUrl();
  }

  var setUrl = function() {
    if (ambiente === 'dev') {
      url = urlLocal;
    } else {
      url = urlS3 + pasta;
    }
  }

  var montaAtributos = function(atributos) {

    var html = '';

    for (var attr in atributos) {
      if (atributos.hasOwnProperty(attr)) {
        if (atributos_externos.indexOf(attr) !== -1) {
          html = html + ' ' + attr + '="' + url + atributos[attr] + '"';
        } else {
          html = html + ' ' + attr + '="' + atributos[attr] + '"';
        }
      }
    }

    return html;
  }

  inicia();

  return {

    amb: function(_ambiente) {
      if (_ambiente === undefined) {
        return ambiente;
      }

      setAmbiente(_ambiente);
      return this;
    },

    pasta: function(_pasta) {
      if (_pasta === undefined) {
        return pasta;
      }
      pasta = _pasta;
      setUrl();
      return this;
    },

    s3: function() {
      return urlS3;
    },

    url: function() {
      return url;
    },

    dom: function(tag) {
      if (tag !== undefined) {
        return getDom(tag);
      }

      return dom;
    },

    img: function(configs) {

      var html = '';

      if (configs !== undefined) {

        html = '<img';

        html = html + montaAtributos(configs);

        html = html + ' />';

      }

      return html;

    },

    a: {
      abre: function(atributos) {

        var html = '';

        if (atributos !== undefined) {
          html = html + '<a';
          html = html + montaAtributos(atributos);
          html = html + '>';
        }

        return html;

      },

      fecha: function() {
        return '</a>';
      }
    },

    arquivo: function(_arquivo) {

      if (_arquivo !== undefined) {
        for (var amb in arquivo) {
          if (_arquivo.hasOwnProperty(amb)) {
            arquivo[amb] = _arquivo[amb];
          }
        }
        return this;
      }

      return arquivo[ambiente];

    },

    arquivos: {

      set: function(nome, fontes) {
        for (var amb in fontes) {
          if (ambientes_permitido.indexOf(amb) !== -1) {
            arquivos[nome] = fontes;
          }
        }
      },

      get: function(nome) {
        return arquivos[nome][ambiente];
      }

    }

  }

}

window._g = oglobo_infograficos();