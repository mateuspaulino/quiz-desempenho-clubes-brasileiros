;(function(){

    _g.arquivos.set('dados', {
        dev: '/csv/dados.csv',
        prod: '/htmlinfo_arquivo/181/dados.csv'
    });

    $(function() {
        var timeout;
        $(window).resize(function() {
            clearTimeout(timeout);
            timeout = setTimeout(function(){
                resize();
                //funcoes do resize
            }, 300);
        });
    });

    function resize() {
      console.log('resize');
    }

    resize();

    var menuTimes = $('.menu-times');

    menuTimes.slick({
        arrows: true,
        centerMode: true,
        centerPadding: '0px',
        slidesToShow:10,
        infinite: true,
        variableWidth: true,
        cssEase: 'linear',
        speed: 200,

    });

}());
